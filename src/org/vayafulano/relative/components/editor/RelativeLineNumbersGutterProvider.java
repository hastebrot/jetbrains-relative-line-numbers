package org.vayafulano.relative.components.editor;

import com.intellij.openapi.editor.Editor;
import org.jetbrains.annotations.Nullable;
import org.vayafulano.relative.RelativeLineNumbersPlugin;

public class RelativeLineNumbersGutterProvider extends NullGutterProvider {

    @Nullable
    @Override
    public String getLineText(int line, Editor editor) {
        if (!RelativeLineNumbersPlugin.isEnabled()) {
            return null;
        }

        if (isCaretInLine(line, editor) && RelativeLineNumbersPlugin.isShowingCurrentLine()) {
            return getLineNumberText(line);
        }

        return getRelativeLineNumberText(line, editor);
    }

    private boolean isCaretInLine(int line, Editor editor) {
        int caretLine = getCaretLine(editor);
        return caretLine - line == 0;
    }

    private int getCaretLine(Editor editor) {
        return editor.getCaretModel().getLogicalPosition().line;
    }

    private String getLineNumberText(int line) {
        return "" + (line + 1);
    }

    private String getRelativeLineNumberText(int line, Editor editor) {
        int currentLine = getCaretLine(editor);
        return "" + Math.abs(currentLine - line);
    }
}

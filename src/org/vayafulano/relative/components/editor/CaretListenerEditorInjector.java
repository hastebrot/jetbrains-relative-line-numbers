package org.vayafulano.relative.components.editor;

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.event.CaretListener;
import com.intellij.openapi.editor.event.EditorFactoryEvent;
import com.intellij.openapi.editor.event.EditorFactoryListener;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.testFramework.LightVirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CaretListenerEditorInjector implements EditorFactoryListener {

    private final CaretListener caretListener;

    public CaretListenerEditorInjector(CaretListener caretListener) {
        this.caretListener = caretListener;
    }

    @Override
    public void editorCreated(@NotNull EditorFactoryEvent event) {
        final Editor editor = event.getEditor();
        if (isFileEditor(editor)) {
            editor.getCaretModel().addCaretListener(caretListener);
        }
    }

    @Override
    public void editorReleased(@NotNull EditorFactoryEvent event) {
        final Editor editor = event.getEditor();
        if (isFileEditor(editor)) {
            editor.getCaretModel().removeCaretListener(caretListener);
        }
    }

    private boolean isFileEditor(@NotNull Editor editor){
        final VirtualFile virtualFile = getVirtualFile(editor);
        return virtualFile != null && !(virtualFile instanceof LightVirtualFile);
    }

    @Nullable
    private static VirtualFile getVirtualFile(@NotNull Editor editor) {
        return FileDocumentManager.getInstance().getFile(editor.getDocument());
    }
}

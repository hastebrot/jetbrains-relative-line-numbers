package org.vayafulano.relative.components.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.ToggleAction;
import org.vayafulano.relative.RelativeLineNumbersPlugin;

public class EnablePluginToggleAction extends ToggleAction {

    @Override
    public boolean isSelected(AnActionEvent anActionEvent) {
        return RelativeLineNumbersPlugin.isEnabled();
    }

    @Override
    public void setSelected(AnActionEvent anActionEvent, boolean b) {
        RelativeLineNumbersPlugin.setEnabled(b);
        RelativeLineNumbersPlugin.refresh();
    }
}

package org.vayafulano.relative.components.settings;

public class PluginSettings {
    private boolean showingCurrentLine = true;
    private boolean enabled = true;

    public boolean isShowingCurrentLine() {
        return showingCurrentLine;
    }

    public void setShowingCurrentLine(boolean showingCurrentLine) {
        this.showingCurrentLine = showingCurrentLine;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}

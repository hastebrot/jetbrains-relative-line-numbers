package org.vayafulano.relative;

import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.editor.TextAnnotationGutterProvider;
import com.intellij.openapi.editor.event.CaretListener;
import com.intellij.openapi.editor.event.EditorFactoryListener;
import org.jetbrains.annotations.NotNull;
import org.vayafulano.relative.components.editor.CaretListenerEditorInjector;
import org.vayafulano.relative.components.editor.EditorGutterRefresher;
import org.vayafulano.relative.components.editor.RelativeLineNumbersGutterProvider;
import org.vayafulano.relative.components.settings.PluginSettings;

@State(
        name = "RelativeLineNumbersSettings",
        storages = {
                @Storage(
                        id = "main",
                        file = "$APP_CONFIG$/vim_settings.xml"
                )}
)
public class RelativeLineNumbersPlugin implements ApplicationComponent, PersistentStateComponent<PluginSettings> {

    private static final String COMPONENT_NAME = "RelativeLineNumbersPlugin";

    private Application application;
    private boolean showingCurrentLine = true;
    private boolean enabled = true;

    public RelativeLineNumbersPlugin(Application application) {
        this.application = application;
    }

    @NotNull
    public static RelativeLineNumbersPlugin getInstance() {
        return (RelativeLineNumbersPlugin) ApplicationManager.getApplication().getComponent(COMPONENT_NAME);
    }

    @Override
    public void initComponent() {
        TextAnnotationGutterProvider gutterProvider = new RelativeLineNumbersGutterProvider();
        CaretListener caretListener = new EditorGutterRefresher(gutterProvider);
        EditorFactoryListener editorFactoryListener = new CaretListenerEditorInjector(caretListener);

        EditorFactory.getInstance().addEditorFactoryListener(editorFactoryListener, application);
    }

    @Override
    public void disposeComponent() {
    }

    @NotNull
    public String getComponentName() {
        return COMPONENT_NAME;
    }

    @Override
    public void loadState(@NotNull PluginSettings pluginSettings) {
        showingCurrentLine = pluginSettings.isShowingCurrentLine();
        enabled = pluginSettings.isEnabled();
    }

    @Override
    public PluginSettings getState() {
        PluginSettings settings = new PluginSettings();
        settings.setShowingCurrentLine(isShowingCurrentLine());
        settings.setEnabled(isEnabled());
        return settings;
    }

    public static boolean isShowingCurrentLine() {
        return getInstance().showingCurrentLine;
    }

    public static void setShowingCurrentLine(boolean showingCurrentLine) {
        getInstance().showingCurrentLine = showingCurrentLine;
    }

    public static boolean isEnabled() {
        return getInstance().enabled;
    }

    public static void setEnabled(boolean enabled) {
        getInstance().enabled = enabled;
    }

    public static void refresh() {
        EditorFactory.getInstance().refreshAllEditors();
    }
}
